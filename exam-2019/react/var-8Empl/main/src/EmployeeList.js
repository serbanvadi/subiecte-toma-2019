import React from 'react';
import AddEmployee from './AddEmployee'

export class EmployeeList extends React.Component {
    constructor(){
        super();
        this.state = {
            employees: []
        };
        this.addElementToList=this.addElementToList.bind(this);
    }
addElementToList(element){
        this.state.employees.push(element);
        this.setState({employees: this.state.employees});
    }
    render(){
        return(
           <div>
                <ul>
                    { 
                    this.state.employees.map(
                        (employee, idx) => 
                        ( <li key={idx}> Name: {employee.name} Surname: {employee.surname} Experience: {employee.experience} </li> )
                        
                    )}
                </ul>
                <AddEmployee onAdd={this.addElementToList}/>
            </div>
        )
    }
}