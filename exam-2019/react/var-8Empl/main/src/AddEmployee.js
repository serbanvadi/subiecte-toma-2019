import React from 'react';

export class AddEmployee extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            name: '',
            surname: '',
            experience: ''
        };
        
           this.handleNameChange = this.handleNameChange.bind(this);
        this.handleSurnameChange = this.handleSurnameChange.bind(this);
        this.handleExperienceChange= this.handleExperienceChange.bind(this);
    }

    addEmployee = (e) => {
        e.preventDefault();
        let employee = {
            name: this.state.name,
            surname: this.state.surname,
            experience: this.state.experience
        };
        this.props.onAdd(employee);
    }
handleNameChange(event){
        this.setState({name: event.target.value});
    }
    
    handleSurnameChange(event){
        this.setState({surname: event.target.value});
    }
    
    handleExperienceChange(event){
        this.setState({experience: event.target.value});
    }
    render(){
        return (
            <div>
           <input type="text" name="name" id="name" value={this.state.name} onChange={this.handleNameChange} placeholder="insert name" />
           <input type="text" name="surname" id="surname" value={this.state.surname} onChange={this.handleSurnameChange} placeholder="insert surname" />
           <input type="text" name="experience" id="experience" value={this.state.experience} onChange={this.handleExperienceChange} placeholder="insert experience" />
           <input type="button" value="add employee" id="btAdd" onClick={this.addEmployee}/>
            </div>
        )
    }
}
export default AddEmployee;