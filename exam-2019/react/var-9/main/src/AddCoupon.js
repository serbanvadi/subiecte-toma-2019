import React from 'react';

export class AddCoupon extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            category: '',
            discount: '',
            availability: ''
        };
        
        this.handleCategoryChange = this.handleCategoryChange.bind(this);
        this.handleDiscountChange = this.handleDiscountChange.bind(this);
        this.handleAvailabilityChange = this.handleAvailabilityChange.bind(this);
    }

    addCoupon = (event) => {
        event.preventDefault();
        let coupon = {
            category: this.state.category,
            discount: this.state.discount,
            availability: this.state.availability
        };
        this.props.onAdd(coupon);
    }
    
    handleCategoryChange(event){
        this.setState({category: event.target.value});
    }
    
    handleDiscountChange(event){
        this.setState({discount: event.target.value});
    }
    
    handleAvailabilityChange(event){
        this.setState({availability: event.target.value});
    }
    
    render(){
        return(
            <div>
                <form className="form-design">
                    <input id="category" name="category" type="text" value={this.state.category} onChange={this.handleCategoryChange} placeholder="Insert category"/>
                    <input id="discount" name="discount" type="text" value={this.state.discount} onChange={this.handleDiscountChange} placeholder="Insert discount"/>
                    <input id="availability" name="availability" type="text" value={this.state.availability} onChange={this.handleAvailabilityChange} placeholder="Insert availability"/>
                  <button onClick={this.addCoupon} value="add coupon">add coupon</button>
                </form>
            </div>
        )
    }
}

export default AddCoupon;