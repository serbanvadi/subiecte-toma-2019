import React from 'react';

export class AddStudent extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            name: '',
            surname: '',
            age: ''
        };
    }

    addStudent = () => {
        let student = {
            name: this.state.name,
            surname: this.state.surname,
            age: this.state.age
        };
        this.props.onAdd(student);
    }

    render(){
        return (
            <div>
           <input type="text" name="name" id="name" />
           <input type="text" name="surname" id="surname" />
           <input type="text" name="age" id="age" />
           <input type="button" value="add student" id="btAdd" onclick="addStudent"/>
            </div>
        )
    }
}