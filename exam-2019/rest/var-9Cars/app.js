// const express = require('express');
// const bodyParser = require('body-parser');
// const cors = require('cors');

// const app = express();

// app.use(bodyParser.json());
// app.use(cors());

// app.locals.cars = [
//     {
//         make: "BMW",
//         model: "X6",
//         price: 50000
//     },
//     {
//         make: "Lamborghini",
//         model: "Huracan",
//         price: 200000
//     },
// ];

// app.get('/cars', (req, res) => {
//     res.status(200).json(app.locals.cars);
// });

// app.post('/cars', (req, res, next) => {
//   if(req.body.constructor === Object && Object.keys(req.body).length === 0 ){
//         res.status(500).json({message:"Body is missing"});
//     }else if(req.body.make === undefined || req.body.model === undefined || req.body.price === undefined){
//         res.status(500).json({message:"Invalid body format"});
//     }else if(req.body.price < 0){
//         res.status(500).json({message:"Price should be a positive number"});
//     }else{
//         let car = {
//             make : req.body.make,
//             model : req.body.model,
//             price : req.body.price
//         };
//         if(isCarAlreadyExist(car)){
//             res.status(500).json({message:"Car already exists"});
//         }else{
//             app.locals.cars.push(car);
//             res.status(201).json({message:"Created"});
//         }
        
//     }
    
    
// })

// function isCarAlreadyExist(car){
//     let found = false;
//     for(var i = 0; i < app.locals.cars.length; i++) {
//         if (app.locals.cars[i].make === car.make && app.locals.cars[i].model === car.model && app.locals.cars[i].price === car.price) {
//             found = true;
//             break;
//         }
//     }
//     return found;
// }

// module.exports = app;
const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');

const app = express();

app.use(bodyParser.json());
app.use(cors());

app.locals.cars = [
    {
        make: "Gigel",
        model: "Popel",
        price: 23
    },
    {
        make: "Gigescu",
        model: "Ionel",
        price: 25
    }
];

app.get('/cars', (req, res) => {
    res.status(200).json(app.locals.cars);
});

app.post('/cars', (req, res, next) => {
    if(req.body.constructor === Object && Object.keys(req.body).length === 0 ){
        res.status(500).json({message:"Body is missing"});
    }else if(req.body.make == undefined || req.body.model == undefined || req.body.price == undefined) {
        res.status(500).json({message:"Invalid body format"});
    }else if(req.body.price < 0 ){
        res.status(500).json({message: "Price should be a positive number"});
    }else {
        let car = {
            make: req.body.make,
            model: req.body.model,
            price: req.body.price
        };
        if(isStudentAlreadyExist(car)){
            res.status(500).json({message:"Car already exists"});
        }else{
            app.locals.cars.push(car);
            res.status(201).json({message:"Created"});
        }
    }
    
})

function isStudentAlreadyExist(car){
    let found = false;
    for(var i = 0; i < app.locals.cars.length; i++) {
        if (app.locals.cars[i].make === car.make && app.locals.cars[i].model === car.model && app.locals.cars[i].price === car.price) {
            found = true;
            break;
        }
    }
    return found;
}

module.exports = app;