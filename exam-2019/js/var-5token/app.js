function textProcessor(input, tokens){
// - `input` should be a `string`. If other type is passed throw an `Error` with the message `Input should be a string`; (0.5 pts)
// - `input` should be at least 6 characters long. If `input` length is less than 6 throw an `Error` with the message `Input should have at least 6 characters`; (0.5 pts)
// - `tokens` is an array with elements with the following format: `{tokenName: string, tokenValue: string}`. If this format is not respected throw an `Error` with the following message `Invalid array format`; (0.5 pts)
// - If `input` don't contain any token return the initial value of `input`; (0.5 pts)
// - If `input` contains tokens, replace them with the specific values and return the new `input`; (0.5 pts)

    if(typeof input!=='string'){
        throw new Error("Input should be a string")
    }
    if(input.length<6){
        throw new Error("Input should have at least 6 characters");
    }
    
    let isValidArray = true;
        for(let token of tokens){
            if(typeof token.tokenName !== "string"){
                isValidArray = false;
                break;
            }
        }
        if(isValidArray == false){
            throw new Error("Invalid array format");
        }
    
    if(input.includes("${name}") == false&& input.includes("${surname}")==false){
        return input;
    }
    else{
        let indexOfToken =0;
                let inputs = input.split(' ');
                for(var i=0; i<inputs.length; i++){
                    if(inputs[i] === "${name}"){
                        inputs[i]=tokens[indexOfToken].tokenValue;
                        indexOfToken++;
                    }
                    else if(inputs[i] === "${surname}."){
                        inputs[i]=tokens[indexOfToken].tokenValue+".";
                        indexOfToken++;
                    }
                }
                console.log(inputs.join(' '));
                return inputs.join(' ');
    //     var result = input.split(' ')
        
    //     for (let i=0; i<result.length;i++){
    //     if(result[i].startsWith('${')){
            
    //         let afterThat = result[i].substring(result[i].indexOf('}')+1, result[i].length)
            
    //         tokens.forEach((token) =>{
    //              if(token.tokenName == result[i].substring(result[i].indexOf('{')+1, result[i].indexOf('}'))){
    //                 result[i] = token.tokenValue
                    
    //             }   
                
    //         });
            
    //         if(afterThat.length>0){
    //             result[i] = result[i] + afterThat
                
    //         }
            
    //     }
        
    // }
        
    // return result.join (' ')
    
    }
}

const app = {
    textProcessor: textProcessor
};

module.exports = app;