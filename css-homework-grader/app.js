'use strict'
const TEMP_DIR = 'tmp'

require('dotenv').config({silent: true})
const fs = require('fs')
const csvtojson = require('csvtojson/v2')()
const git = require('simple-git/promise')(TEMP_DIR)
const rimraf = require('rimraf')
const backstop = require('backstopjs')


let main = async () => {
	if (process.argv.length < 4){
		console.warn('usage : node app.js <input-csv> <comparison-target>')
		process.exit(1)
	}
	// prep
	let csvInput = process.argv[2]
	let targetDir = process.argv[3]
	targetDir = targetDir.endsWith('/') ? targetDir.slice(0, targetDir.length - 1) : targetDir
	console.warn('creating temp dir')
	rimraf.sync('./' + TEMP_DIR)
	rimraf.sync('./backstop-data/bitmaps_reference')
	rimraf.sync('./backstop-data/bitmaps_test')
	rimraf.sync('./backstop-data/html_report')
	fs.mkdirSync('./' + TEMP_DIR)
	let spec = fs.readFileSync(targetDir + '/backstop.json', 'utf-8')
	let jsonSpec = JSON.parse(spec)
	let submissions
	try{
		submissions = await csvtojson.fromFile(csvInput)
	}
	catch (e){
		console.warn(e.stack)
	}
	if (!submissions){
		console.warn('no submissions')
		process.exit(1)
	}
	jsonSpec.scenarios[0].referenceUrl = `./${targetDir}/index.html`
	// jsonSpec.scenarios[0].referenceUrl = './sample-target/index.html'
	fs.writeFileSync('./backstop.json', JSON.stringify(jsonSpec, null, 2), 'utf-8')
	await backstop('reference')
	// process repos
	let results = []
	for (let item of submissions){
		let result = {}
		result.email = item.email
		result.name = item.name
		try{
			console.warn('cloning : ' + item.repo)
			const remote = `https://${encodeURIComponent(process.env.GIT_USER)}:${encodeURIComponent(process.env.GIT_PASSWORD)}@${item.repo}`
			let repoName = item.repo.match(/.*\/(.*)\.git/)[1]
			await git.clone(remote)
			// generate testing harness
			jsonSpec.scenarios[0].url = `./${TEMP_DIR}/${repoName}/${item.dir}/index.html`
			// jsonSpec.scenarios[0].url = './tmp/test_tw_homework/homework1/index.html'
			fs.writeFileSync('./backstop.json', JSON.stringify(jsonSpec, null, 2), 'utf-8')
			// run tests
			await backstop('test')
			result.status = 'done'

		}
		catch(e){
			if (e.message.endsWith('Mismatch errors found.')){
				result.status = 'error'
			}
			else{
				console.warn(e.stack)
				result.status = 'inconclusive'
			}
		}
		// process.exit(1)
		// record results
		let reportContent = fs.readFileSync('backstop_data/html_report/config.js', 'utf-8')
		let reportText = reportContent.slice(7, reportContent.length - 2)
		let jsonReport = JSON.parse(reportText)
		let rate = jsonReport.tests.filter((e) => e.status !== 'fail').length / jsonReport.tests.length
		result.rate = rate
		results.push(result)
	}
	console.warn(results)	
}

try{
	main()
}
catch(e){
	console.warn(e.stack)
}


