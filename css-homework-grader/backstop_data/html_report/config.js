report({
  "testSuite": "BackstopJS",
  "tests": [
    {
      "pair": {
        "reference": "../bitmaps_reference/backstop_default_scenario_1_0_header_0_comp.png",
        "test": "../bitmaps_test/20181103-103348/backstop_default_scenario_1_0_header_0_comp.png",
        "selector": "header",
        "fileName": "backstop_default_scenario_1_0_header_0_comp.png",
        "label": "scenario 1",
        "misMatchThreshold": 0.1,
        "url": "./tmp/test_tw_homework/homework1/index.html",
        "referenceUrl": "./sample-target/index.html",
        "expect": 0,
        "viewportLabel": "comp",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "2.76",
          "analysisTime": 9
        },
        "diffImage": "../bitmaps_test/20181103-103348/failed_diff_backstop_default_scenario_1_0_header_0_comp.png"
      },
      "status": "fail"
    },
    {
      "pair": {
        "reference": "../bitmaps_reference/backstop_default_scenario_1_1_body_0_comp.png",
        "test": "../bitmaps_test/20181103-103348/backstop_default_scenario_1_1_body_0_comp.png",
        "selector": "body",
        "fileName": "backstop_default_scenario_1_1_body_0_comp.png",
        "label": "scenario 1",
        "misMatchThreshold": 0.1,
        "url": "./tmp/test_tw_homework/homework1/index.html",
        "referenceUrl": "./sample-target/index.html",
        "expect": 0,
        "viewportLabel": "comp",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "0.09",
          "analysisTime": 28
        }
      },
      "status": "pass"
    },
    {
      "pair": {
        "reference": "../bitmaps_reference/backstop_default_scenario_1_2_articlesectionfirst-child_0_comp.png",
        "test": "../bitmaps_test/20181103-103348/backstop_default_scenario_1_2_articlesectionfirst-child_0_comp.png",
        "selector": "article section:first-child",
        "fileName": "backstop_default_scenario_1_2_articlesectionfirst-child_0_comp.png",
        "label": "scenario 1",
        "misMatchThreshold": 0.1,
        "url": "./tmp/test_tw_homework/homework1/index.html",
        "referenceUrl": "./sample-target/index.html",
        "expect": 0,
        "viewportLabel": "comp",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "0.00"
        }
      },
      "status": "pass"
    },
    {
      "pair": {
        "reference": "../bitmaps_reference/backstop_default_scenario_1_3_articlesection_0_comp.png",
        "test": "../bitmaps_test/20181103-103348/backstop_default_scenario_1_3_articlesection_0_comp.png",
        "selector": "article section",
        "fileName": "backstop_default_scenario_1_3_articlesection_0_comp.png",
        "label": "scenario 1",
        "misMatchThreshold": 0.1,
        "url": "./tmp/test_tw_homework/homework1/index.html",
        "referenceUrl": "./sample-target/index.html",
        "expect": 0,
        "viewportLabel": "comp",
        "diff": {
          "isSameDimensions": true,
          "dimensionDifference": {
            "width": 0,
            "height": 0
          },
          "misMatchPercentage": "0.00"
        }
      },
      "status": "pass"
    }
  ],
  "id": "backstop_default"
});